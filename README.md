## Through The Ages: A New Story Of Civilization

### Doel Van Het Spel

#### Meeste cultuur aan het eind van de 4e 'EEUW'

- Iedereen speelt tot het eind
- Je kan elkaar aanvallen, maar niet vernietigen
- De uitdaging is om de juiste balans tussen de ONTWIKKELINGEN en INVLOEDEN te vinden en daar het maximale uit te halen
- SPELVERLOOP: zie spelerskaart & quick reference, of zie [Een quick reference van een fan](https://silverhammermba.github.io/throughtheages/)

### 3 soorten invloeden

- CULTUUR
	- Speler met hoogste CULTUUR wint
	- Speelt een rol bij sommige EVENTS
- LEGERMACHT/STRENGTH
	- Speelt een rol bij de meeste POLITIEK
	- Speelt een rol bij veel EVENTS
- WETENSCHAP
	- Gebruikt om TECH te kopen
	- Speelt een rol bij sommige EVENTS  

----

- Invloeden worden bij wijziging direct bijgewerkt op de bijbehorende track
- Voor CULTUUR en WETENSCHAP is ook een track voor het aantal punten dat een speler krijgt tijdens zijn PRODUCTIEFASE

----

### 3 soorten middelen

- ACTIES & MILITAIRE ACTIES
	- Aantal is vooral afhankelijk van REGERINGSVORM
		- 2 manieren om te wijzigen:
			- Vredige wijziging: Veel WETENSCHAP en 1 ACTIE
			- Na wijziging heb je direct beschikking over extra ACTIES & MILITAIRE ACTIES, behalve bij een revolutie:
			- Revolutie: Weinig WETENSCHAP en max aantal ACTIES (je kan dus geen gewone ACTIES doen in een beurt met een revolutie, MILITAIRE ACTIES zijn wel beschikbaar)
	- Je begint elke beurt met je max aantal
	- Alles wat je doet kost ACTIES of MILITAIRE ACTIES, aangeduid met de ACTIE TOKEN (grijs) of de MILITAIRE ACTIE TOKEN (rood)
- POPULATIE
	- Beperkt door het aantal TOKENS in je POPULATIE BANK met POPULATIE TOKENS (geel) 
		- De BANK kan met specifieke ACTIES worden uitgebreid
	- Wordt na elke EEUW schaarser
	- Ongebruikte POPULATIE wordt in de WORKER POOL bewaard
	- Beperkt door TEVREDENHEID van je populatie
		- TEVREDENHEID wordt bij een tekort aangevuld met ongebruikte POPULATIE
		- TEVREDENHEID kan worden verbeterd met ONTWIKKELINGEN en INVLOEDEN
		- Als je aan het eind van je beurt ontevreden POPULATIE hebt, VERVALT JE PRODUCTIEFASE!
	- Toevoegen van POPULATIE aan je WORKER POOL uit de BANK wordt aangeduid met een BRUINE PIJL icoon
	- In EEUW A zijn sommige EVENTS alleen nuttig als je een ongebruikte TOKEN hebt in je WORKER POOL, het is daarom een goed idee om je tweede beurt af te sluiten met een extra POPULATIE TOKEN
- GRONDSTOFFEN/VOEDSEL
	- Zijn beperkt door het aantal TOKENS in je GRONDSTOFFEN BANK
	- Wordt na elke EEUW schaarser
	- Elke speler heeft zijn eigen BANK van GRONDSTOFFEN/VOEDSEL TOKENS (blauw)
		- De BANK an met specifieke ACTIES worden uitgebreid
	- GRONDSTOFFEN/VOEDSEL wordt aan het eind van elke beurt bijgewerkt, afhankelijk van:
		- Vermogen van je PRODUCTIEGEBOUWEN
		- Corruptie(bij een tekort aan blauwe TOKENS in je bank)
		- Drempelwaardes in je POPULATIEBANK, die de hoeveelheid verbruikt VOEDSEL van je POPULATIE aangeven
		- Ontevreden POPULATIE (bij een tekort aan TEVREDENHEID in je POPULATIE BANK)

### Ontwikkeling

#### CIVIL CARDS

- CIVIL CARDS worden gekocht met ACTIES
- X aantal CIVIL CARDS aan de linkerkant van de kaartenreeks vervallen na elke beurt, afhankelijk van het aantal spelers
- kaartenreeks wordt aan het begin van elke beurt aangevuld uit het CIVIL DECK vanaf de rechterkant en worden goedkoper na verloop van tijd
- Als het CIVIL DECK leeg is, begint de volgende EEUW (Meestal terwijl de kaartenreeks wordt aangevuld aan het begin van een beurt)
	- Als de volgende EEUW begint, worden alle kaarten in je hand van twee EEUWen terug uit het spel verwijderd
	- Het overgebleven MILITARY DECK van de huidige EEUW gaat uit het spel
	- Het CIVIL DECK en het MILITARY DECK van de volgende EEUW worden geschud en komen in het spel
	- Eventueel wordt de kaartenreeks aangevuld met CIVIL CARDS uit het nieuwe CIVIL DECK 
- Aantal CIVIL CARDS in je hand is beperkt door je max aantal ACTIES
	- Als je MAX aantal CIVIL CARDS hebt, mag je geen CIVIL CARDS kopen
	- Koop alleen CIVIL CARDS die je wilt spelen, want er is geen makkelijke manier om CIVIL CARDS af te leggen
- 4 soorten CIVIL CARDS (ACTIEKAART, WONDER, LEIDER & TECH)
	- TECH
		- Kost 1 ACTIE om gespeeld te worden
		- Je mag niet meer dan 1 van dezelfde TECH kaart in je hand of in het spel hebben
		- TECH kaarten worden aangemerkt met een gloeilamp icoon in de linkerbovenhoek
		- TECH kosten WETENSCHAP, zoals aangeduid op de kaart
		- Nieuwe BUILDINGS moeten eerst worden gekocht, alvorens ze gebouwd mogen worden m.b.v. POPULATIE TOKENS
			- Max aantal POPULATIE TOKENS per URBAN BUILDING wordt beperkt door REGERINGSVORM (2 tot 4)
			- Elke GRONDSTOFFEN TOKEN op een MINE of FARM is gelijk aan het aantal GRONDSTOFFEN/VOEDSEL dat een gebouw produceert
				- v.b.: een TOKEN op een EEUW A FARM is 1 VOEDSEL waard, terwijl een TOKEN op een EEUW III FARM 5 VOEDSEL waard is
	- ACTIEKAART
		- Mag pas in je volgende beurt gespeeld worden
		- Kost 1 ACTIE om gespeeld te worden
		- Wordt uitgespeeld en daarna uit het spel verwijderd
		- Instructies op een ACTIEKAART die normaal gesproken een ACTIE kosten, zijn altijd gratis
	- WONDER
		- Gaat na aankoop niet naar je hand, maar komt op je tableau in 'aanbouw' (op zijn kant)
		- Kost geen extra ACTIE om gespeeld te worden
		- De voordelen van een voltooid WONDER staan in iconen afgebeeld of beschreven op de kaart
		- De voordelen van een wonder gaan pas van kracht na voltooiing van alle bouwstages, v.l.n.r.
			- Elke bouwstage is uitgedrukt met een getal, dat het aantal benodigde GRONDSTOFFEN voorstelt
			- Elke bouwstage kost standaard 1 ACTIE
			- De voortgang van een WONDER wordt bijgehouden met een GRONDSTOFFEN TOKEN uit je eigen BANK
				- GRONDSTOFFEN TOKENS gaan na voltooiing terug naar je eigen BANK
	- LEIDER
		- Is alleen gratis te spelen als het een bestaande LEIDER vervangt, anders kost het 1 ACTIE
		- Vervangen leiders worden uit het spel verwijderd
		- Je kan maar 1 LEIDER per EEUW bezitten

### Invloed
Verkregen uit:  

	- Leger eenheden
	- TACTIEKEN
	- LEIDER
	- TECH
		- BELEID
		- REGERINGSVORM
		- URBAN BUILDINGS
	- KOLONIES
	- WONDEREN

#### MILITARY CARDS
- MILITARY CARDS krijg je aan het eind van je beurt
	- Controleer aan het eind van je beurt eerst of je meer dan je max aantal MILITAIRE ACTIES aan MILITARY CARDS hebt
		- Zo ja, gooi kaarten weg tot je net zo veel MILITARY CARDS als max MILITAIRE ACTIES hebt
		- Trek daarna kaarten uit het MILITARY DECK gelijk aan het aantal overgebleven MILITAIRE ACTIES deze beurt
- MILITARY DECK wordt pas aangevuld aan het begin van een nieuwe EEUW (als het CIVIL DECK op raakt)
- 3 soorten MILITARY CARDS (BONUS, TACTIEK & POLITIEK):
	- BONUS
		- BONUS kaarten kunnen worden uitgespeeld voor 2 doelen:
			- Ter verdediging van aggressie (bovenste deel)
			- Extra invloed bij KOLONIE veilingen (onderste deel)
	- TACTIEK
		- Worden aangeduid met een MILITAIRE ACTIE TOKEN in de linkerbovenhoek
		- Kosten 1 MILITAIRE ACTIE om te spelen
		- Geeft een bonus in je LEGERMACHT/STRENGTH voor elke overeenkomende set van leger eenheden
		- Wordt een openbare TACTIEK aan het begin van je volgende beurt
		- Je mag een openbare TACTIEK kopieren voor 2 MILITAIRE ACTIES
		- Tactieken vanaf EEUW 2 hebben een 'sterke' en een 'zwakke' LEGERMACHT/STRENGTH. De sterke variant is alleen geldig als de gebruikte legers voor deze tactiek uit deze EEUW of de vorige EEUW komen.
	- POLITIEK
		- Worden aangemerkt met een kroontje in de linkerbovenhoek
		- Je mag aan het begin van je beurt 1 POLITIEK kaart spelen
		- Beinvloeden andere spelers als volgt:
			- Oorlogsverklaring
			- Pact
			- Aggressie
			- EVENT/KOLONIE VOORBEREIDEN


##### EVENT/KOLONIE VOORBEREIDEN
	- aangemerkt met een CULTUUR icoon rechtsboven
	- Een EVENT/KOLONIE wordt op het FUTURE EVENTS DECK gelegd, het effect wordt op een later moment uitgespeeld
	- Levert CULTUUR punten op gelijk aan het getal van de EEUW op de achterzijde
	- Een EVENT/KOLONIE uit de CURRENT EVENTS DECK wordt uitgespeeld nadat een kaart aan de FUTURE EVENTS DECK is toegevoegd
	    - Na 6 events is het CURRENT EVENTS DECK leeg, en wordt het FUTURE EVENTS DECK geschud en neergelegd als het nieuwe CURRENT EVENTS DECK
        - Aan het einde van EEUW IV worden de resterende events in het CURRENT EVENTS DECK achter elkaar uitgespeeld
		- Als er een KOLONIE wordt uitgespeeld, kan deze in een veiling gekocht worden met 1..* LEGERMACHT/STRENGTH + * KOLONIEMACHT

###### KOLONIE VEILING
	- Je maximale bod is gebaseerd op je totale LEGERMACHT/STRENGTH (minimaal 1 leger eenheid) + je totale KOLONIEMACHT (uit kaarten op je tableau en/of BONUS kaarten in je hand)
		- De LEGERMACHT/STRENGTH punten van je huidige TACTIEK tellen mee in je maximale bod
	- De eerste speler doet een bod, waarna spelers met de klok mee het bod mogen overtreffen of passen
		- Een speler valt uit de veiling als hij past
	- Het winnende bod moet worden betaald
		- Betalen van een KOLONIE kost minimaal 1 LEGER en eventueel BONUS kaarten
			- KOLONIEMACHT op je tableau hoeft niet worden ingeleverd
			- De leger eenheden gaan terug naar je POPULATIE BANK, de BONUS kaarten gaan uit het spel
		- KOLONIE geeft een eenmalige beloning (in de beschrijving) en een permanente beloning (aangeduid met iconen onderaan de kaart)

